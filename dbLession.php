<?php

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
$globalConfig = yaml_parse_file(__DIR__.'/env.yml');
$dbAssets = $globalConfig['database'];

$mysqli = new mysqli($dbAssets['host'], $dbAssets['user'], $dbAssets['password'], $dbAssets['database']);

// Check connection
if ($mysqli -> connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
    exit();
}



//Creating
function create(mysqli $mysqli) {
    $insert1 = $mysqli->query(
        "INSERT INTO `task` 
            (`title`, `description`, `note`, `status`, `user_id`, `project_id`)
            VALUES ('My first task', 'We need to bla-bla-bla', NULL, '0', '1', '1');"
    );
    $insert2 = $mysqli->query(
        "INSERT INTO `task` 
            (`title`, `description`, `note`, `status`, `user_id`, `project_id`)
            VALUES ('My second task', 'We need to bla-bla-bla', NULL, '0', '1', '2');"
    );
    $insert3 = $mysqli->query(
        "INSERT INTO `task` 
        (`title`, `description`, `note`, `status`, `user_id`, `project_id`)
        VALUES ('My third task', 'We need to bla-bla-bla', NULL, '0', '1', '2');"
    );
}

// List

function getList(mysqli $mysqli) {
    $result = $mysqli->query("SELECT * FROM task");

//    while ($object = $result->fetch_assoc()) {
//        echo '<pre>';
//        var_dump($object);
//        echo '</pre>';
//    }

//    echo '<pre>';
//    var_dump($result->fetch_array());
//    echo '</pre>';


    echo '<pre>';
    var_dump($result->fetch_all());
    echo '</pre>';
}

function getOne(mysqli $mysqli)
{
    $result = $mysqli->query("SELECT * FROM task WHERE `description` = 'We need to bla-bla-bla' LIMIT 5");

    echo '<pre>';
    var_dump($result->fetch_all());
    echo '</pre>';

}

// Editing
function update(mysqli $mysqli) {
    $result3 = $mysqli->query("UPDATE `task` SET
        `status` = '2'
        WHERE `id` = '1';"
    );


    echo '<pre>';
    var_dump($result3);
    echo '</pre>';
}


//create($mysqli);
//getList($mysqli);
//getOne($mysqli);
//update($mysqli);


function createProjects(mysqli $mysqli) {
    $mysqli->query("
        CREATE TABLE `project` (
            `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
            `title` varchar(86) NOT NULL
        );
    ");

    $mysqli->query("
        INSERT INTO `project` (`title`)
        VALUES ('Project 1');"
    );
    $mysqli->query("
        INSERT INTO `project` (`title`)
        VALUES ('Project 2');"
    );

    $result = $mysqli->query("SELECT * FROM project");
    echo '<pre>';
    var_dump($result->fetch_all());
    echo '</pre>';
}

//createProjects($mysqli);

function joinSelect(mysqli $mysqli) {
    $result = $mysqli->query("SELECT `project`.`title`, `task`.`title`, `task`.`id`
        FROM `project`
        LEFT JOIN `task`
        ON `project`.`id` = `task`.`project_id`"
    );

    echo '<pre>';
    var_dump($result->fetch_all());
    echo '</pre>';
}

joinSelect($mysqli);
