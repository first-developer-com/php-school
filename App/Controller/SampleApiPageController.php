<?php

namespace App\Controller;

use FirstDeveloperCom\Router\Http\Request\RequestInterface;
use FirstDeveloperCom\Router\Http\Response\ResponseInterface;

final class SampleApiPageController extends AbstractController
{
    public function __construct(RequestInterface $request, ResponseInterface $response)
    {
        parent::__construct($request, $response);
    }

    public function execute(): ResponseInterface
    {

        $this->response->setBody(['key' => 'value']);
        return $this->response;
    }
}
