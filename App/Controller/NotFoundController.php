<?php

namespace App\Controller;

class NotFoundController
{
    public function execute()
    {
        echo "404";

        header('HTTP/1.0 404 Not Found');
        exit();
    }
}
