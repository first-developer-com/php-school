<?php

namespace App\Controller;

use FirstDeveloperCom\Router\Http\Request\RequestInterface;
use FirstDeveloperCom\Router\Http\Response\ResponseInterface;

abstract class AbstractController
{
    protected RequestInterface $request;
    protected ResponseInterface $response;

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     */
    public function __construct(RequestInterface $request, ResponseInterface $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    abstract public function execute(): ResponseInterface;
}
