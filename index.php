<?php
use FirstDeveloperCom\Router\Config;
use FirstDeveloperCom\Router\Http\Request\RequestCreator;
use FirstDeveloperCom\Router\Router;

require __DIR__ . '/vendor/autoload.php';
require_once "Autoloader.php";

Autoloader::register();

$globalConfig = yaml_parse_file(__DIR__.'/env.yml');


$routesConfig = yaml_parse_file(__DIR__ . '/' . $globalConfig['routes']['path']);

$routerConfig = new Config(
    new RequestCreator(),
    __DIR__ . '/' . $globalConfig['routes']['path'],
    $globalConfig['controllers']['namespace'],
    $globalConfig['NotFoundController']['namespace']
);

$router = new Router($routerConfig);
$router->run();
